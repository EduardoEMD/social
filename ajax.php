<?php require_once "model.php";
	require_once "ajax_class.php";
$magic = new Ajax();
if(isset($_POST['newuser'])){
	$magic->register();

}
if(isset($_POST['userlog'])){
	$magic->log_in();
}

if(isset($_POST['userSearch'])){
	$magic->srch();
}
if(isset($_POST['friendrequest'])){
	$magic->send_request();

}
if(isset($_POST['frienddel'])){
	$magic->delete_friend();
}

if(isset($_POST['req-quest']) && $_POST['req-quest']=='?'){
	$magic->check_new_request();
}

if(isset($_POST['fr-req-who']) && isset($_POST['fr-req-ans'])){
	$magic->request_answer();
}
if(isset($_POST['msg_with'])){
	$magic->activate_chat_with();
}
if(isset($_POST['msg_text'])){
	$magic->insert_message();
}
if(isset($_POST['chat_ready'])){
	$magic->chet_ready();
	
}
if(isset($_POST['friends-list'])){
	$magic->get_friends();
}
if(isset($_POST['new-event'])){
	$magic->new_event_checking();
}

?>