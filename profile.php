<?php require_once "model.php";
$id = isset($_SESSION['userID'])? $_SESSION['userID']:-1;
if($id == -1){
	header('location: error.php');
}

$test->set_table("user");
$user = $test->find(array('id' => $id));
$friends = $test->get_friends($id);
$nkar = isset($user[0]['avatar'])? $user[0]['avatar']:'images/avatar.png';

?>
<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){
   
    $img = $_FILES['nkar'];
    if($img['name'] != ""){
        $hasce = "images/".$img['name'];
        move_uploaded_file($img["tmp_name"], $hasce);
        $test->set_table("user");
        $test->update($id,array("avatar"=>"$hasce"));
        Header('Location:'.$_SERVER['PHP_SELF']);

    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/profile.css">
</head>
<body>
	<header id="out-of-space">
		<a href="index.php?logout"><img src="images/logout.png" ></a>
		<div id="events">
			<ul>
				<li id="events-request"><img src="images/friends.png" ></li>
				<li id="events-message"><img src="images/messages.png" ></li>
			</ul>
		</div>
		<div class="searcher">
			<input type="text" placeholder='Search ...'><button>&#128269</button>
		</div>
	</header>
	<div class="bg-cover">
		<div class="cover-info">
			
		</div>
	</div>
	<div id="prof-head">
		<div id="avt-div">
			<img src=<?= $nkar ?> class="img-thumbnail">
			<form id="nkar1" action="profile.php" method="post" enctype="multipart/form-data">
				<input id="get-photo" name="nkar" type="file" >
				<input id="use-img" type="submit" value="Change Avatar">
			</form>
		</div>
		<div id="prof-events">
			<h1><?= $user[0]['name']." ".$user[0]['surname'] ?></h1>
			<button>- - -</button>
			<button>- - -</button>
			<button>- - -</button>
		</div>
	</div>
	<div id="prof-cont">
		<div id="nav">
			<ul>
				<li>Photos</li>

				<li>Friends</li>

				<li>The Basics</li>

				<li>Information</li>
			</ul>
		</div>
		<div id="nav-bar-out">

			<h1>About</h1>
			<div id="nav-1">

			</div>
			<h1>Contact</h1>
			<div id="nav-2">

			</div>
		</div>
		<div id="most-inc">

		</div>
	</div>
	<div id="chat">
		<div id="friend-list" class="col-xs-4 col-md-2">
          <?php for($i = 0;$i<count($friends); $i++): ?>

            <img width="50" height="50" src=<?php if(isset($friends[$i]['avatar'])){print $friends[$i]['avatar'];}else{print 'images/avatar.png';}?>>
            <label data-id=<?=  $friends[$i]['id'] ?>><?= $friends[$i]['name'].' '.$friends[$i]['surname'] ?> </label>
            <br>
            <br>
        <?php endfor; ?>
		</div>
		<div class="container">
    <div class="row chat-window col-xs-5 col-md-3" id="chat_window_1" style="margin-left:10px;">
        <div class="col-xs-12 col-md-12">
        	<div class="panel panel-default">
                <div class="panel-heading top-bar">
                    <div class="col-md-8 col-xs-8">
                        <span class="glyphicon glyphicon-comment"></span> <h3 class="panel-title"> Chat</h3>
                    </div>
                    <div class="col-md-4 col-xs-4" style="text-align: right;">
                       
                    </div>
                </div>
                <div class="panel-body msg_container_base">
                   
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                    </div>
                </div>
    		</div>
        </div>
    </div>
    
   
</div>
	</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/profile.js"></script>
</html>
