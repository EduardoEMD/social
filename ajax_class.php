<?php 
	require_once "model.php";
	class Ajax{
		private $model;

		public function __construct(){
			$this->model = new Model();
			$this->model->select_database("esoc");
			$this->model->set_table("user");
		}
		public function new_event_checking(){
			$id = $_SESSION['userID'];
			$unread_message = $this->model->isset_new_message($id);
			$this->model->set_table('requests');
			$unread_request = $this->model->find(array('user2'=> $id));
			$events = array('mes' => $unread_message,'req'=>$unread_request);
			print json_encode($events);
		}

		public function get_friends(){
			$id = $_SESSION['userID'];
			$friends = $this->model->get_friends($id);
			print json_encode($friends);
		}

		public function chet_ready(){
			$friend = $_SESSION['active_friend'];
			$me = $_SESSION['userID'];
			$chat = $this->model->get_messages($me,$friend);
			$this->model->message_saw($me,$friend);
			print json_encode($chat);
		}
		public function insert_message(){
			$mysqli = new mysqli("localhost", "root", "", "esoc");
			$to = $_SESSION['active_friend'];
			$from = $_SESSION['userID'];
			$text = $_POST['msg_text']; 
			$text =  $mysqli->real_escape_string($text);
			$time = $_POST['msg_time'];
			$this->model->set_table('messages');
			$this->model->insert(array('user1_id'=>$from,'user2_id'=>$to,"text"=>$text,'time'=>$time,'status'=>0));
		}

		public function activate_chat_with(){
			$_SESSION['active_friend'] = $_POST['msg_with'];
		}

		public function request_answer(){
			$me = $_SESSION['userID'];
			$reqfrom = $_POST['fr-req-who'];
			$reqans = $_POST['fr-req-ans'];
			$this->model->set_table('requests');
			$this->model->delete(array('user1' => $reqfrom,'user2'=>$me));
			$this->model->delete(array('user1' => $me,'user2'=>$reqfrom));
			if($reqans == 1){
				$this->model->set_table('friends');
				$this->model->insert(array('user1' => $reqfrom,'user2'=>$me));
			}
		}
		public function check_new_request(){
			$id = $_SESSION['userID'];
			$this->model->set_table('requests');
			$fid = $this->model->find(array('user2'=> $id));

			$this->model->set_table('user');
			$result = array();
			for($i = 0; $i < count($fid); $i++){
				$result[] = $this->model->find(array('id'=> $fid[$i]['user1']));
			}

			 print json_encode($result);
				}
		
	public function delete_friend(){
		$user = $_SESSION['userID'];
		$whom = $_POST['frienddel'];
		$this->model->set_table('friends');
		$this->model->delete(array('user1'=>$user,'user2'=>$whom));
		$this->model->delete(array('user1'=>$whom,'user2'=>$user));
	}
	public function send_request(){
		$to = $_POST['friendrequest'];
	 	$from = $_SESSION['userID'];
	 	$this->model->set_table("requests");
	 	$this->model->insert(array('user1' => $from,'user2'=>$to));
	}
	public function srch(){
		$data = $_POST['userSearch'];
	 	$order = explode(" ", $data);
	 	$all = $this->model->findAll();
		$result = array();
		
		for($i = 0; $i < count($all); $i++){
			for($j = 0; $j < count($order); $j++){
				if(strtolower($all[$i]['name']) == strtolower($order[$j]) || strtolower($all[$i]['surname']) == strtolower($order[$j]) ){
					$result[] = $all[$i];

				}
			}
		}
		$end = array();
		for($i=0; $i <count($result) ; $i++) { 
			if($result[$i]['id'] != $_SESSION['userID']){
				$end[] = $result[$i];

			}
		}
		print json_encode($end);
	}

	public function log_in(){
		$email = $_POST['userlog'][0];
		$pass = $_POST['userlog'][1];
		$hashed_password = $this->model->find(array('email'=>$email));

		if(!empty($email) && !empty($pass)){
			$user = $this->model->find(array('email'=>$email));
			if(isset($user) && count($user)>0 && password_verify($pass, $hashed_password[0]['password'])){
				$id = $user[0]['id'];
				$_SESSION['userID'] = $id;
				print 0;
				
			}else{
				print 'Your email or password is wrong !';
				
			}
		}else if(empty($email) && !empty($pass)){
			print 'Please enter your email !';


		}else if(!empty($email) && empty($pass)){
			print 'Please enter your password !';


		}else {
			print 'Please enter your data !';

		}
	}

	public function register(){
		$first = $_POST['newuser'][0];
		$second = $_POST['newuser'][1];
		$email = $_POST['newuser'][2];
		$pass = $_POST['newuser'][3];

		$match = $this->model->find(array("email"=>$email));
		$hashed_password = password_hash($pass, PASSWORD_DEFAULT);
		if(!empty($first) && !empty($second) && !empty($email) && !empty($pass) && count($match) == 0 && !filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			$this->model->insert(array('name' => $first,'surname'=> $second,'email'=>$email, 'password' => $hashed_password));
			print 'Congratulations you have been registered successfully';
		}else if(empty($first) || empty($second) || empty($email) || empty($pass)){
			print 'Please fill all fields correctly';
		}else if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			print "Your email is not correct";
		}else if(count($match)>0){
			print 'Your email is already in use';
		}
	}
}

?>