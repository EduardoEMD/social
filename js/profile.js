$(document).ready(function(){

setInterval(function(){
	ajaxCall('ajax.php','post',"new-event",'json').then(function(r){

		if(r['mes'].length > 0){
			$('#events-message img').attr('src',"images/new_messages.png")
			for(i = 0; i<r['mes'].length;i++){
				console.log(r['mes'][i])
				$("#chat label").each(function(){
					
					if( r['mes'][i] == $(this).attr('data-id') ){
						$(this).addClass('new-something')
					}
				})
			}
		}else{
			$('#events-message img').attr('src',"images/messages.png")
		}

		if(r['req'].length > 0){
			$('#events-request img').attr('src','images/new_friends.png')
		}else{
			$('#events-request img').attr('src','images/friends.png')
		}

	})
	
},1500)

$(window).scroll(function() {
	if($(document).scrollTop() > 0){
		$('#out-of-space').css("opacity",0.5)
	}else{
		$('#out-of-space').css("opacity",1)
	}

});
$('body').on("click", "#events ul li:nth-child(2)",function(){
	$('#chat').toggle(300)
})
$('body').on('click','#nav ul li:nth-child(2)',function(){
	ajaxCall('ajax.php','post',"friends-list",'json').then(function(r){
		$('html,body').animate({ scrollTop: $('#most-inc').offset().top }, 300);

		$("#most-inc").empty()
		
		for(i = 0; i < r.length; i++){

			var nkar = r[i]['avatar']
			if(nkar == undefined){
				nkar = "images/avatar.png"
			}
			var photo = $("<img width='50' height='50' class='fr-list-avt'>")
			photo.attr('src',nkar)
			var elm = $("<div class='col-md-4'></div>")
			var link = $("<a></a>")
			$(link).attr('href','guest.php?id='+r[i]['id'])
			$(link).append("<h3>"+r[i]['name']+" "+r[i]['surname']+"</h3>")
			$(elm).append(photo)
			$(elm).append(link)
			
			
			var btn = $("<button class='fr-list-del'>Delete Friend</button>")
			$(btn).attr('data-list',i)
			$(btn).attr('data-id',r[i]['id'])
			$(elm).append(btn)
			$(elm).append("<hr>")
			$("#most-inc").append(elm)

		}
	})
})

$('body').on('click','.fr-list-del',function(){
	var id = $(this).attr('data-id')
	var index = $(this).attr('data-list')
	$("#most-inc div").eq(index).fadeOut(300)
	ajaxCall('ajax.php','post',{"frienddel":id})
})
$('#avt-div img').bind('click', function(e){
	$("#use-img").show();
	$('#get-photo').click();
	

})
$('.cover-info').on('click','#close-info',()=>{
	$('.bg-cover').hide()
})

$('body').on('click','#chat label',function(){
	$(this).removeClass("new-something");
	$('.msg_container_base').empty()
	$('.panel-title').empty()
	$('.panel-title').html($(this).text())
	var data = {"msg_with":$(this).attr('data-id')}
	ajaxCall("ajax.php","post",data)
	setInterval(function(){
		ajaxCall("ajax.php",'post','chat_ready','text').then(function(r){
			var data = JSON.parse(r);
			
			var me = data[0]['user1_id'];
			$('.msg_container_base').empty()
			
			for(i=0; i<data.length; i++){
				var elm1 = $("<div'></div>")
				var elm2 = $("<div class='col-md-10 col-xs-10'></div>")
				var elm3 = $("<div></div>")
				if(data[i].user1_id == me){
					$(elm1).attr('class', 'base_sent')
					$(elm3).attr('class','msg_me messages msg_sent')
				}else{
					$(elm1).attr('class', 'row msg_container base_receive')
					$(elm3).attr('class','messages msg_recive')
				}
				var elm4 = $("<div class='col-md-2 col-xs-2 avatar'><div>")
				$(elm4).append("img src='images/avatar.png'")
				$(elm3).append(data[i].text)
				$(elm3).appendTo(elm2)
				$(elm2).appendTo(elm1)
				$('.panel-body').append(elm1)
			}
			
		})
	}, 500);

})


$(".searcher button").click(function(){
	var data = $(".searcher input").val()
	ajaxCall('ajax.php','post', {"userSearch":data},'json').then(function(r){
		$(".cover-info").empty()
		$('.cover-info').append('<ins id="close-info">&#10006</ins>')
		$('.bg-cover').fadeIn(500)
		if(r.length > 1){
			for(i = 0; i<r.length ;i++){
				for( j=i;j<r.length;j++){
					if(r[i].id == r[j].id){
						r.splice(i,1)
					}
				}
			}
		}
		for(i = 0; i < r.length; i++){
			var nkar = r[i]['avatar']
			if(nkar == undefined){
				nkar = "images/avatar.png"
			}
			var link = $("<a></a>")
			link.attr("href","guest.php?id="+r[i]['id'])
			var photo = $("<img width='50' height='50'>")
			photo.attr('src',nkar)
			$(link).append(photo)
			$(link).append("<h3>"+r[i]['name']+" "+r[i]['surname']+"</h3>")
			$(link).append("<hr>")
			$(".cover-info").append(link)
		}
	})
})


$("#events ul li:first-child()").click(function(){
	ajaxCall('ajax.php','post',{"req-quest":"?"},'json').then(function(r){
		$(".cover-info").empty()
		$('.cover-info').append('<ins id="close-info">&#10006</ins>')
		$('.bg-cover').fadeIn(500)
		for(i = 0; i < r.length; i++){
			var nkar = r[i][0]['avatar']
			if(nkar == undefined){
				nkar = "images/avatar.png"
			}
			var photo = $("<img width='50' height='50' >")
			photo.attr('src',nkar)
			var elm = $("<span></span>")
			$(elm).append(photo)
			$(elm).append("<h3>"+r[i][0]['name']+" "+r[i][0]['surname']+"</h3>")
			var btn1 = $("<button class='req-ans req-acpt'>Accept</button>")
			$(btn1).attr('data-list',i)
			$(btn1).attr('data-id',r[i][0]['id'])
			$(btn1).attr('data-req',1)
			$(elm).append(btn1)
			var btn2 = $("<button class='req-ans req-decl'>Decline</button>")
			$(btn2).attr('data-list',i)
			$(btn2).attr('data-id',r[i][0]['id'])
			$(btn2).attr('data-req',0)
			$(elm).append(btn2)
			$(elm).append("<hr>")
			$(".cover-info").append(elm)

		}
	})

})


$("body").on('click','.req-ans',function(){
	var index = $(this).attr('data-list')
	$(".cover-info span").eq(index).fadeOut(300)
	var id = $(this).attr('data-id')
	var ans = $(this).attr('data-req')
	ajaxCall('ajax.php','post',{'fr-req-who':id,'fr-req-ans':ans})

})
$("#btn-input").keydown(function(event){
	$('.msg_container_base').animate({
		scrollTop: $('.msg_container_base').get(0).scrollHeight
	}, 0);
    if(event.keyCode == 13){
    	$("#btn-chat").click()        
    }
});
$('#btn-chat').click(()=>{
	$('.msg_container_base').animate({
		scrollTop: $('.msg_container_base').get(0).scrollHeight
	}, 0);
	var msg = $('#btn-input').val()
	$('#btn-input').val("")

	if(msg!=""){
		var time = Math.round(Date.now()/1000);
		
		ajaxCall('ajax.php','post',{'msg_text':msg,"msg_time":time}).then(function(){
			$('.msg_container_base').animate({
				scrollTop: $('.msg_container_base').get(0).scrollHeight
			}, 0);
		});
	}

})
$('#btn-input').click(()=>{
	$('.msg_container_base').animate({
		scrollTop: $('.msg_container_base').get(0).scrollHeight
	}, 0);
})



function ajaxCall(url,type,data,dataType='json'){
	return $.ajax({
		type:type,
		url:url,
		data: data,
		dataType:dataType

	})
}


})
