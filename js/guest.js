$(document).ready(function(){
	$(".searcher button").click(function(){
		var data = $(".searcher input").val()
		ajaxCall('ajax.php','post',{"userSearch":data},'json').then(function(r){
			$(".cover-info").empty()
			$('.bg-cover').fadeIn(500)
			for(i = 0; i < r.length; i++){

				var nkar = r[i]['avatar']
				if(nkar == undefined){
					nkar = "images/avatar.png"
				}
				var link = $("<a></a>")
				link.attr("href","guest.php?id="+r[i]['id'])
				var photo = $("<img width='50' height='50'>")
				photo.attr('src',nkar)
				$(link).append(photo)
				$(link).append("<h3>"+r[i]['name']+" "+r[i]['surname']+"</h3>")
				$(link).append("<hr>")
				$(".cover-info").append(link)
			}
		})


	})

	

	$('.add').click(function(){
		var id = getParameterByName('id');
		ajaxCall('ajax.php','post',{"friendrequest":id}).then(function(){
			location.reload();
		})
	
	})
	$(".del-frnd").click(function(){
		var id = getParameterByName('id');
		ajaxCall('ajax.php','post',{"frienddel":id}).then(function(){
			location.reload();
		})
	})
	$('.del-req').click(function(){
		var id = getParameterByName('id');
		ajaxCall('ajax.php','post',{"fr-req-who":id,"fr-req-ans":0}).then(function(){
			location.reload();
		})
	})

})
function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function ajaxCall(url,type,data,dataType='html'){
	return $.ajax({
		type:type,
		url:url,
		data: data,
		dataType:dataType

	})
}
