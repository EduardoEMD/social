	
<?php
session_start();
class Model{
	private $conn;
	private $table;

	function __construct(){
		$this->conn= new mysqli('localhost','root','');
	}

	public function set_table($x){
		$this->table = $x;
	}
	public function select_database($x){
		$this->conn->select_db($x);
	}

	public function findAll(){
		
		return $this->conn->query("SELECT * FROM  $this->table")->fetch_all(MYSQLI_ASSOC);
	}

	public function find(Array $x){

		$where = '';
		$last = count($x);
		$i = 0;

		foreach($x as $a => $b){
			$where.= "$a='$b'";
			if($i!=$last-1){
				$where.=" and ";
			}  
			$i++;
		}

		return  $this->conn->query("SELECT * FROM $this->table WHERE $where")->fetch_all(MYSQLI_ASSOC);
	}

	public function insert(Array $x){
		$keys = '(';
		$values = '(';
		$last = count($x);
		$i = 0;
		foreach($x as $a => $b){
			$keys.= "$a";
			$values.= "'$b'";
			if($last == $i+1){
				$keys.= ')';
				$values.= ')';
			}
			if($last != $i+1){
				$keys.= ' , ';
				$values.= ' , ';
				$i++;
			}
		}

		$this->conn->query("INSERT into $this->table $keys values $values");

	}
	public function delete(Array $x){
		$where = '';
		$last = count($x);
		$i = 0;
		foreach ($x as $key => $value) {
			$where.= "$key='$value' ";
			if($last != $i+1){
				$where.= " and ";
			}
			$i++;
		}

		$this->conn->query("DELETE FROM $this->table where $where");


	}

	public function update($id,Array $x){
		$where = '';
		$last = count($x);
		$i = 0;
		foreach ($x as $key => $value) {
			$where.= "$key='$value' ";
			if($last != $i+1){
				$where.= " , ";
			}
			$i++;
		}

		$this->conn->query("UPDATE $this->table set $where where id=$id");
	}

	public function get_tables(){
		$a = $this->conn->query("show tables")->fetch_all();
		for($i = 0; $i<count($a); $i++){
			$b[] = $a[$i][0];
		}
		return $b;

	}

	public function get_databases(){
		$a = $this->conn->query("show databases")->fetch_all();
		for($i = 0; $i<count($a); $i++){
			$b[] = $a[$i][0];
		}
		return $b;

	}

	public function exist_inf($x,$y){
		$a = $this->conn->query("SELECT * from $this->table where user1=$x and user2=$y")->fetch_all(MYSQLI_ASSOC);
		$b = $this->conn->query("SELECT * from $this->table where user1=$y and user2=$x")->fetch_all(MYSQLI_ASSOC);
		if(count($a) > count($b)){
			return $a;
		}else{
			return $b;
		}
	}

	public function get_friends($x){
		$this->table = 'messages';
		$a = $this->conn->query("select * from user where id in(
			select user1 from friends where user2=$x
			UNION
			SELECT user2 from friends where user1=$x)")->fetch_all(MYSQLI_ASSOC);
		return $a;

	}
	public function get_messages($x,$y){
		$this->table = 'messages';
		$a = $this->conn->query("select * from messages where user1_id = $x and user2_id = $y
			UNION
			select * from messages where user1_id = $y and user2_id = $x order by time 
			")->fetch_all(MYSQLI_ASSOC);
		return $a;
	}
	public function message_saw($x,$y){
		$this->conn->query("update messages set status=1 where user2_id=$x and user1_id=$y");
	}

	public function isset_new_message($x){
		$a = $this->conn->query("select DISTINCT user1_id from messages where user2_id=$x and status =0")->fetch_all(MYSQLI_ASSOC);
		if(count($a)>0){
		for($i = 0; $i < count($a); $i++){
			$b[] = $a[$i]['user1_id'];
		}
		return $b;

		}else{
			return $a;
		}
	}
	

} 

$test = new Model();
$test->select_database("esoc");
	// $test->set_table("dasaxos");
	// $a = $test->find(array("name"=>"Hamlet","surname"=>"Harutyunyan"));
	// $test->insert(array('name' => 'Naira','surname'=>'Zohrabyan','ambion_id'=>'2')); // dont forget foreign_keys + not null 
	// $test->delete(array('name' => 'Naira','surname'=>'Zohrabyan','ambion_id'=>'2'));
	// $test->update(1,array("name"=>"Hamlet","surname"=>"Harutyunyan"));
	// $a = $test->get_tables();
	// $c = $test->get_databases();


?>