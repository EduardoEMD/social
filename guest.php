<?php require_once "model.php";
$id = isset($_GET['id'])? $_GET['id']:-1;

if($id == -1 || !isset($_SESSION['userID'])){
	header('location: error.php');
}else{
	$test->select_database("esoc");
	$test->set_table("user");
	$guest = $test->find(array('id' => $id));

	$nkar = isset($guest[0]['avatar'])? $guest[0]['avatar']:'images/avatar.png';

	$test->set_table("friends");
	$arefriend = $test->exist_inf($_SESSION['userID'],$id);
	$test->set_table("requests");
	$isset_req = $test->exist_inf($_SESSION['userID'],$id);

}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/profile.css">
	

</head>
<body>
	<header id="out-of-space">
		<a href="index.php?logout"><img src="images/logout.png" ></a>
		<a href="profile.php"><img id="go-home" src="images/home.png" style="float: left;margin-left: 3%"></a>
		<div class="searcher" style="margin-top: 20px;margin-left: 280px">
			<input type="text" placeholder='Search ...'><button type="">&#128269</button>
		</div>
	</header>
	
	<div id="prof-head">
		<div id="avt-div">
			<img src=<?= $nkar ?> class="img-thumbnail">
			<form id="nkar1" action="profile.php" method="post" enctype="multipart/form-data">
				<input id="get-photo" name="nkar" type="file" >
				<input id="use-img" type="submit" value="Change Avatar">
			</form>
		</div>
		
		<div id="prof-events">
			<h1><?= $guest[0]['name']." ".$guest[0]['surname'] ?></h1>

		<?php if(count($arefriend)>0): ?>
			<button class="del-frnd">Delete Friend</button>
		<?php elseif(count($arefriend)==0 && count($isset_req)>0 ): ?>
			<button class="del-req">Delete Request</button>
		<?php else: ?>
			<button class="add">Add Friend</button>
		<?php endif; ?>

			
			
		</div>
	</div>
	

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/guest.js"></script>

</html>



